#!/usr/bin/env python3

import pymongo
import chess
import chess.pgn
import sys
import re
import os
import io

pid = os.getpid()

badGames = []
badPgns  = [] 

def getMainLine(node):
    moves = []
    while node.variations:
        next_node = node.variation(0)
        moves.append(node.board().san(next_node.move))
        node = next_node

    return moves

def fixGame(gameData):
    replaced = re.sub('(\S[^\s=x])([QNBR])([\+#]{0,1}[\s\n])', '\\1=\\2\\3', gameData)
    return replaced

def extractGamesFromFile(fileName):
    pgn = open(fileName, 'r')
    pgnData = pgn.read()
    pgn.close()
    
    return [fixGame('[Event ' + game) for game in pgnData.split('[Event ')][1:]

def parseGame(game):
    headers = game.headers

    parsed = headers.copy()
    parsed['raw'] = str(game)
    parsed['moves'] = getMainLine(game)

    post_id = collection_parsed.insert_one(parsed).inserted_id

def parseFile(fileName):
    print("Parsing file %s" % fileName)

    try:
        games = extractGamesFromFile(fileName)
    except Exception as e:
        badPgns.append(fileName)
        print("Bad encoding (probably)! Use iconv -f ... -t utf-8")
        print(e)
        return

    for index, gamePgn in enumerate(games):
        if index % 10 == 0:
            print("Parsing game: %d/%d, failed total: %d" % (index + 1, len(games), len(badGames)))
        
        try:
            game = chess.pgn.read_game(io.StringIO(gamePgn))
        except Exception as e:
            badGames.append(gamePgn)
        else:
            parseGame(game)

def startMongo():
    global collection_parsed
    
    mongo = pymongo.MongoClient('localhost', 27017)
    db = mongo['chessdb']
    collection_parsed = db['games']

def parseDir(dirName):
    print("Parsing dir %s" % dirName)

    for root, directories, filenames in os.walk(dirName):
        for filename in filenames:
            if filename[-4:] == '.pgn': 
                path = os.path.join(root, filename)
                parseFile(path)

def parseList(listFile):
    print("Parsing list %s" % listFile)

    data = open(listFile, 'r').read()
    for file in data.split('\n'):
        if file == '': continue
        parseFile(file)

def main(args):
    print("Connecting to Mongodb...")
    startMongo()

    print("Start parsing...")
    
    mode = args[0] if args else None

    if mode == "--files":
        files = args[1:]

        for file in args[1:]:
            parseFile(file)
    elif mode == "--dir":
        parseDir(args[1])
    elif mode == "--list":
        parseList(args[1])
    else:
        print("Use --files or --dir")

def saveLogs():
    bagGamesFile = open('badGames-%d.pgn' % pid, 'w')
    for badGame in badGames:
        bagGamesFile.write(badGame + "\n\n")

    bagPgnsFile = open('badPgns-%d.txt' % pid, 'w')
    for badPgn in badPgns:
        bagPgnsFile.write(badPgn + "\n")

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print("\nBye!")
    except Exception as e:
        print("Exit with exception: %s" % str(e))
    finally:
        print("Parsing was ended. We ended with %d fails." % len(badGames))
        saveLogs()
