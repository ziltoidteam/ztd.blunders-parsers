#!/bin/bash

FROM=$1
TO=$2

echo "Source $FROM"
echo "Destination $TO"


find $FROM -type f -iname "*.pgn" -exec bash -c 'echo {} && iconv -t utf8 "{}" -o "/mnt/USB/ChessData-encoded/$(basename "{}")" || iconv -f CP1250 -t utf8 "{}" -o "/mnt/USB/ChessData-encoded/$(basename "{}")"' \;
