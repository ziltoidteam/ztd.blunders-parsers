#!/usr/bin/env python3

import chardet
import sys
import os

gameBuffer  = []
splitCount  = 100
fileCounter = 1
encodeSearchWindow = 1000

def extractGames(content):
    print("    Extracting games")

    return ['[Event ' + game for game in content.split('[Event ')][1:]

def forceReadAsUnicode(fileName):
    try:
        return open(fileName, 'r').read()
    except UnicodeError as e:
        start = max(e.start - encodeSearchWindow, 0)
        end   = e.end + encodeSearchWindow

        file = open(fileName, 'rb')
        rawData = file.read()
        file.close()

        encoding = chardet.detect(rawData[start: end])['encoding']

        unicodeData = rawData.decode(encoding = encoding, errors = 'replace')

        return unicodeData

def processBuffer(force = False):
    global gameBuffer
    global fileCounter

    while len(gameBuffer) >= (1 if force else splitCount):
        portion = gameBuffer[:splitCount]

        gameBuffer = gameBuffer[splitCount:]

        output = open('%d.pgn' % fileCounter, 'w')
        for game in portion:
            output.write(game + '\n')

        if fileCounter % 10 == 0:
            print('Created %d files' % fileCounter)

        output.close()

        fileCounter += 1

def parseFile(fileName):
    global gameBuffer

    print("Prepairing %s" % fileName)

    unicodeData = forceReadAsUnicode(fileName)

    gameBuffer += extractGames(unicodeData)

    processBuffer()

def parseDir(dirName):
    for root, directories, filenames in os.walk(dirName):
        for filename in filenames:
            if filename[-4:] == '.pgn': 
                path = os.path.join(root, filename)
                parseFile(path)

def parseList(listFile):
    data = open(listFile, 'r').read()
    for file in data.split('\n'):
        if file == '': continue
        parseFile(file)

def main(args):
    mode = args[0] if args else None

    if mode == "--files":
        files = args[1:]

        for file in args[1:]:
            parseFile(file)
    elif mode == "--dir":
        parseDir(args[1])
    elif mode == "--list":
        parseList(args[1])
    else:
        print("Use --files or --dir")

    processBuffer(force = True)

if __name__ == "__main__":
    main(sys.argv[1:])
