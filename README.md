# README #

This repository contains a set of utilities for parsing pgn files and finding errors in them and chess problems in them.

## Try it online

Ztd.Blunders available in [blunders.ztd.io](http://blunders.ztd.io) and this is main server site.

### Summary ###

* prepare.py - a module that reads and combines pgn files, fix file encoding, and then divide them into equal portions

* parser-to-mongo.py - a module that reads pgn files prepared, highlights of which we are interested in the information and stores it in the form of database records MongoDb

* blunder-finder.py - a module detects an error and chooses the chess problem. It uses chess engine Stockfish open source to analyze games

### How I use parsers? ###

### Contribution guidelines ###

### Third-party tools and data ###