#!/usr/bin/env python3

import pymongo
import sys
import random
import chess
import chess.uci
import time

portionSize = 10
pathToEngine = "Stockfish/src/stockfish"
infinity = 100000

checkTime = 2000
mutiScoreTime = 1000

minimalErrorDelta = 290
minMultiScoreDelta = 250
tooBadScore = 9 * 100

def pvToStr(fen, move, pv):
    board = chess.Board(fen)
    board.push(board.parse_san(move))

    result = []
    for move in pv:
        movestr = board.san(move)
        result.append(movestr)
        board.push(move)

    return result
    
def saveBlunder(fenBefore, blunderMove, pgnId, move_index, forcedLine, pv, elo):
    blunder = {}
    blunder['pgn_id']      = pgnId
    blunder['move_index']  = move_index

    blunder['fenBefore']   = fenBefore
    blunder['blunderMove'] = blunderMove
    
    blunder['forcedLine']  = pvToStr(fenBefore, blunderMove, forcedLine)
    blunder['pv']          = pvToStr(fenBefore, blunderMove, pv)

    blunder['elo']         = elo

    filtered_blunders_collection.insert(blunder)

    print("BLUNDER! (elo: %s, fen: %s)" % (elo, fenBefore))

def scoreToInt(score):
    if score.mate is not None:
        if score.mate > 0:
            return +infinity
        
        return -infinity
    
    return score.cp

def getScore(engine, timeToThink):
    engine.isready()
    engine.go(infinite = True)
    
    time.sleep(timeToThink / 1000)
    
    tries = 0
    score = None
    pv = None
    while score is None or pv is None:
        with handler:
            if "score" in handler.info and "pv" in handler.info:
                score = scoreToInt(handler.info["score"])
                pv = handler.info["pv"][1]
            else:
                tries += 1
                time.sleep(timeToThink / 1000 / 5)
                if tries >= 100:
                    score = None
                    pv = None
                    break

    engine.stop()
    engine.isready()

    return pv, score

def getMultiScore(scoreNeeds, timeToThink):
    engine.isready()
    engine.setoption({'MultiPV': scoreNeeds})

    engine.isready()
    engine.go(infinite = True)

    time.sleep(timeToThink / 1000)
    
    tries = 0
    scores = None
    while scores is None or len(scores) != scoreNeeds:
        with handler:
            if "score_multi" in handler.info:
                scores = handler.info["score_multi"]
            else:
                tries += 1
                time.sleep(timeToThink / 1000 / 5)
                if tries >= 100:
                    scores = None
                    break

    result = [scoreToInt(score) for score in scores.values()]

    engine.stop()
    engine.isready()

    engine.setoption({'MultiPV': 1})
    engine.isready()

    return result

def getForcedLine(board, pv):
    forcedLine = []
    complexity = 0

    ourMove = True
    for move in pv:
        if len(board.legal_moves) >= 2 and ourMove:
            engine.position(board)
            score1, score2 = getMultiScore(2, mutiScoreTime)
            difference = score1 - score2

            if difference <= minMultiScoreDelta: break

            complexity += 1 / difference
        
        forcedLine.append(move)
        board.push(move)
        ourMove = not ourMove

    # Removing last move, if last move is enemy move
    if len(forcedLine) % 2 == 0: forcedLine = forcedLine[:-1]

    elo = int(1200 + complexity * 100000) + (len(forcedLine) - 1) * 30
    return elo, forcedLine

def filterBlunder(blunder):
    pgnId = blunder['pgn_id']
    move_index = blunder['move_index']

    fenBefore = blunder['fenBefore']
    blunderMove = blunder['blunderMove']

    board = chess.Board(fenBefore)
    engine.position(board)
    _1, beforeScore = getScore(engine, checkTime)

    board.push(board.parse_san(blunderMove))
    engine.position(board)
    pv, afterScore = getScore(engine, checkTime)

    if beforeScore + afterScore < minimalErrorDelta:
        print('Not a blunder (error delta too low)')
        return

    if beforeScore == -infinity and afterScore == infinity:
        print('All scores is mate. Not intresting')
        return

    if beforeScore < -tooBadScore:
        print('Too bad position. Not intresting')
        return

    elo, forcedLine = getForcedLine(board, pv)
    
    if len(forcedLine) == 0:
        print('Zero forced line - first move is not forced!')
        return

    saveBlunder(fenBefore, blunderMove, pgnId, move_index, forcedLine, pv, elo)
    
def parsePortion(portion):
    unparsed = []

    for blunder in portion:
        blunders_collection.update(blunder, {"$set": {"magicKey": 1}})
        unparsed.append(blunder)

    try:
        while len(unparsed) > 0:
            filterBlunder(unparsed[-1])
            unparsed.pop()
    except (KeyboardInterrupt, Exception) as e:
        if isinstance(e, Exception):
            print("Exception! (%s)" % str(e))

        print("\nWait a little bit to stop and save data...")

        for blunder in unparsed:
            blunders_collection.update(blunder, {"$unset": {"magicKey": 1}}, False, True)
        
        return False

    return True

def startEngine():
    global engine
    global handler

    engine = chess.uci.popen_engine(pathToEngine)
    engine.uci()
    handler = chess.uci.InfoHandler()
    engine.info_handlers.append(handler)
    engine.isready()

    engine.setoption({'MultiPV': 1})
    engine.isready()

    if not engine.is_alive():
        return False

    return True

def startMongo():
    global blunders_collection
    global filtered_blunders_collection

    mongo = pymongo.MongoClient('localhost', 27017)
    blunders_collection = mongo['chessdb']['blunders']
    filtered_blunders_collection = mongo['chessdb']['filtered_blunders']

def main(args):
    random.seed()

    if not startEngine():
        print("Can't connect to engine")
        return

    startMongo()

    while True:
        collectionSize = blunders_collection.count({"magicKey": {"$exists": 0}})
        
        if collectionSize == 0:
            print("No more blunders. Bye!")
            break

        portionStart = random.randrange(0, collectionSize)
        
        portion = blunders_collection.find({"magicKey": {"$exists": 0}}).skip(portionStart).limit(portionSize)

        if not parsePortion(portion):
            break

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Bye!')
