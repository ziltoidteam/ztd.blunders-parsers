#!/usr/bin/env python3

import pymongo
import chess
import chess.pgn
import chess.uci
import io
import time
import os
import sys
import random

portionSize = 10

pathToEngine = "Stockfish/src/stockfish"
minimalBlunderDepth = 8
infinity = 100000

minimalErrorDelta = 250
minimalBestmoveDelta = 200

timeToCheckError = 100
timeToCheckMultiPv = 100

def saveBlunder(fenBefore, errorDelta, bestmoveDelta, blunderMove, pgn_id, move_index, totalMoves, pv):
    blunder = {}
    blunder['fenBefore']     = fenBefore
    blunder['errorDelta']    = errorDelta
    blunder['bestmoveDelta'] = bestmoveDelta
    blunder['blunderMove']   = blunderMove
    blunder['pgn_id']        = pgn_id
    blunder['move_index']    = move_index
    blunder['pv']            = pv

    blunders.insert(blunder)

    print("%s: BLUNDER!, processed moves %d/%d" % (str(pgn_id), move_index, totalMoves))
    
    epd.write(fenBefore + " bm " + blunderMove + "; id " + str(pgn_id) + ";\n")
    epd.flush()

def scoreToInt(score):
    if score.mate is not None:
        if score.mate > 0:
            return +infinity
        
        return -infinity
    
    return score.cp

def getScore(engine, timeToThink):
    engine.isready()
    engine.go(infinite = True)
    
    time.sleep(timeToThink / 1000)
    
    tries = 0
    score = None
    pv = None
    while score is None or pv is None:
        with handler:
            if "score" in handler.info and "pv" in handler.info:
                score = scoreToInt(handler.info["score"])
                pv = handler.info["pv"][1]
            else:
                tries += 1
                time.sleep(timeToThink / 1000 / 5)
                if tries >= 100:
                    score = None
                    pv = None
                    break

    engine.stop()
    engine.isready()

    return pv, score

def getMultiScore(scoreNeeds, timeToThink):
    engine.isready()
    engine.setoption({'MultiPV': scoreNeeds})

    engine.isready()
    engine.go(infinite = True)

    time.sleep(timeToThink / 1000)
    
    tries = 0
    scores = None
    while scores is None or len(scores) != scoreNeeds:
        with handler:
            if "score_multi" in handler.info:
                scores = handler.info["score_multi"]
            else:
                tries += 1
                time.sleep(timeToThink / 1000 / 5)
                if tries >= 100:
                    scores = None
                    break

    result = [scoreToInt(score) for score in scores.values()]

    engine.stop()
    engine.isready()

    engine.setoption({'MultiPV': 1})
    engine.isready()

    return result

def findBlunders(gameRecord):
    engine.ucinewgame()

    data = gameRecord['raw']
    moves = gameRecord['moves']

    board = chess.Board(chess.STARTING_FEN)

    scores = []
    for index, move in enumerate(moves):

        uci_move = board.parse_san(move)
        board.push(uci_move)
        if board.is_game_over():
            break

        fen = board.fen()

        if index < minimalBlunderDepth - 1: 
            scores.append((0, fen))
            continue

        engine.isready()
        engine.position(board)

        pv, score = getScore(engine, timeToCheckError)
        if score is None:
            print("%s: WARNING (Engine is not answers), game was skipped" % str(gameRecord['_id']))
            return

        if index % 2 == 0:
            score = -score

        scores.append((score, fen))

        if index < minimalBlunderDepth: 
            continue

        score, fen = scores[-1]
        prevScore, prevFen = scores[-2]

        sign = -1 if (index % 2 == 0) else 1

        errorDelta = score - prevScore
        if sign * errorDelta >= minimalErrorDelta: 
            if len(board.legal_moves) <= 1:
                print("%s: WARNING: Only one legal move" % str(gameRecord['_id']))
                return

            multiScores = getMultiScore(2, timeToCheckMultiPv)
            if multiScores is None:
                print("%s: WARNING: Engine is not answers, game was skipped" % str(gameRecord['_id']))
                return

            bestmoveDelta = multiScores[0] - multiScores[1]

            if bestmoveDelta >= minimalBestmoveDelta:
                parsedPv = []
                for move in pv:
                    movestr = board.san(move)
                    parsedPv.append(movestr)
                    board.push(move)

                saveBlunder(prevFen, errorDelta, bestmoveDelta, moves[index], gameRecord['_id'], index, len(moves), parsedPv)
                return
            else:
                break

    print("%s: ok, processed moves %d/%d" % (str(gameRecord['_id']), index, len(moves)))

def startEngine():
    global engine
    global handler

    engine = chess.uci.popen_engine(pathToEngine)
    engine.uci()
    handler = chess.uci.InfoHandler()
    engine.info_handlers.append(handler)
    engine.isready()

    engine.setoption({'MultiPV': 1})
    engine.isready()

    if not engine.is_alive():
        return False

    return True

def startMongo():
    global mongo
    global blunders
    global games_collection

    mongo = pymongo.MongoClient('localhost', 27017)
    db = mongo['chessdb']
    games_collection = db['games']
    blunders = db['blunders']

def parseGamesPortion(games_portion):
    games_unparsed = []

    for game in games_portion:
        games_collection.update(game, {"$set": {"magicKey": 1}})
        games_unparsed.append(game)

    try:
        while len(games_unparsed) > 0:
            game = games_unparsed[-1]
            
            findBlunders(game)
            games_unparsed.pop()
    except (KeyboardInterrupt, Exception) as e:
        if isinstance(e, Exception):
            print("Exception! (%s)" % str(e))

        print("\nWait a little bit to stop and save data...")

        for game in games_unparsed:
            games_collection.update(game, {"$unset": {"magicKey": 1}}, False, True)
        
        return False

    return True

def main(args):
    random.seed()

    global epd

    pid = os.getpid()
    epd = open('blunders-%d.epd' % pid, 'w')

    if not startEngine():
        print("Can't connect to engine")
        return

    startMongo()

    while True:
        collectionSize = games_collection.count({"magicKey": {"$exists": 0}})
        
        if collectionSize == 0:
            print("No more games. Bye!")
            break

        portionStart = random.randrange(0, collectionSize)

        games_portion = games_collection.find({"magicKey": {"$exists": 0}}).skip(portionStart).limit(portionSize)

        if not parseGamesPortion(games_portion):
            break

if __name__ == "__main__":
    main(sys.argv[1:])
